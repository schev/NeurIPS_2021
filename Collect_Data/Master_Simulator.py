# Test Simulation
import numpy as np
import matplotlib.pyplot as plt
import os
import sys

# Add functions to path
os.chdir(os.path.dirname(__file__))
sys.path.append("..\Functions")

from RK_Trap_fJ       import RK_ft, RK_Jt
from RK_Trap_Sim      import RKT_Sim
from Newton_Solver    import NS

# Which system will we simulate?
tst = "K"

if tst == "C":
    from Cubic_Oscillator import f, J
    x0 = np.array([0.8, -0.4])
elif tst == "L":
    from Lorenz import f, J
    x0 = np.array([-10, -10, 18])
else:
    from Kundur import f, J, x0f
    x0 = x0f().flatten()
    x0[0] = x0[0] + 0.01

# Define Simulation Parameters
print(x0)
if tst == "C":
    dt = 0.001
    t0 = 0
    tf = 50
    Newt_Tol = 1e-8;
    Collect_Data = 0
    Eu_step      = 1
    data = RKT_Sim(x0,dt,t0,tf,f,J,RK_ft,RK_Jt,NS,Newt_Tol,Collect_Data,Eu_step)
    plt.plot(data[0,:])
    plt.plot(data[1,:])
    plt.show()
elif tst == "L":
    dt = 0.001
    t0 = 0
    tf = 10
    Newt_Tol = 1e-8;
    Collect_Data = 0
    Eu_step      = 1
    data = RKT_Sim(x0,dt,t0,tf,f,J,RK_ft,RK_Jt,NS,Newt_Tol,Collect_Data,Eu_step)
    plt.plot(data[0,:])
    plt.plot(data[1,:])
    plt.plot(data[2,:])
    plt.show()
else:
    dt = 0.01
    t0 = 0
    tf = 10
    Newt_Tol = 1e-8;
    Collect_Data = 0
    Eu_step      = 1
    data = RKT_Sim(x0,dt,t0,tf,f,J,RK_ft,RK_Jt,NS,Newt_Tol,Collect_Data,Eu_step)
    plt.plot(data[4,:])
    plt.plot(data[5,:])
    plt.plot(data[6,:])
    plt.plot(data[7,:])
    plt.show()
    print(data[4,1])
