# Test Simulation
import numpy as np
import copy
import matplotlib.pyplot as plt
import sys
import csv
import pickle
import math
import os

# Add functions to path
os.chdir(os.path.dirname(__file__))
sys.path.append("..\Functions")

# Define Random Seed
np.random.seed(1)

from RK_Trap_fJ       import RK_ft, RK_Jt
from RK_Trap_Sim      import RKT_Sim
from Newton_Solver    import NS

# Kundur system
from Kundur import f, J, x0f
x0 = x0f().flatten()
x0[0:4]  = x0[0:4]  + 2*math.pi
x0[8:10] = x0[8:10] + 2*math.pi

# Define Simulation Parameters
dt = 0.001
t0 = 0
tf = 2.5
Newt_Tol = 1e-9
ColDat   = 1
Eu_step  = 0

# Run Simulation
for ii in range(40):
    # Perturb initial conditions
    x0p       = copy.copy(x0)
    x0p[0:4]  = x0p[0:4]  + 0.002*np.random.randn(4)
    x0p[8:10] = x0p[8:10] + 0.002*np.random.randn(2)
    print(x0p)

    # Simualte and collect data
    data, NewtStps_Param, NewtStps_In, NewtStps_Out = RKT_Sim(x0p,dt,t0,tf,f,J,RK_ft,RK_Jt,NS,Newt_Tol,ColDat,Eu_step)

    # Subtract the rotor angle
    data[4:8,:]           = data[4:8,:]           - x0[5]
    NewtStps_Param[4:8,:] = NewtStps_Param[4:8,:] - x0[5]

    plt.plot(data.T)
    print(NewtStps_Param[0:10])
    #plt.plot(NewtStps_Param.T)
    #plt.plot(NewtStps_In.T)
    #plt.plot(NewtStps_Out.T)

    # Aggregate
    if ii == 0:
        Data_Sim   = copy.copy(data)
        Data_SimIn = [len(data[0,:])]
        Data_Param = copy.copy(NewtStps_Param)
        Data_In    = copy.copy(NewtStps_In)
        Data_Out   = copy.copy(NewtStps_Out)
    else:
        Data_Sim   = np.concatenate((Data_Sim,data),1)
        Data_SimIn = np.concatenate((Data_SimIn,[len(data[0,:])]))
        Data_Param = np.concatenate((Data_Param,NewtStps_Param),1)
        Data_In    = np.concatenate((Data_In,NewtStps_In),1)
        Data_Out   = np.concatenate((Data_Out,NewtStps_Out),1)

# Now, throw all of the data into a list, and then export as a pickle
Master_List = [Data_Sim, Data_SimIn, Data_Param, Data_In, Data_Out]
plt.show()
#with open("Kundur_Data.pkl", 'wb') as f:
#        pickle.dump(Master_List, f)
