# Test Simulation
import numpy as np
import copy
import matplotlib.pyplot as plt
import sys
import csv
import pickle
import os

# Add functions to path
os.chdir(os.path.dirname(__file__))
sys.path.append("..\Functions")

# Define Random Seed
np.random.seed(1)

from RK_Trap_fJ       import RK_ft, RK_Jt
from RK_Trap_Sim      import RKT_Sim
from Newton_Solver    import NS

# Cubic Oscillator
from Hopf import f, J
x0 = np.array([0.1, -0.5, -0.5])

# Define Simulation Parameters
dt = 0.025
t0 = 0
tf = 45
Newt_Tol = 1e-9
ColDat   = 1
Eu_step  = 0

# Run Simulation
x0 = np.array([0.1, -0.5, -0.5])
for ii in range(50):
    # Perturb initial conditions
    x0p = copy.copy(x0)
    x0p = x0p + 0.25*np.random.randn(3)
    print(x0p)

    # Simualte and collect data
    data, NewtStps_Param, NewtStps_In, NewtStps_Out = RKT_Sim(x0p,dt,t0,tf,f,J,RK_ft,RK_Jt,NS,Newt_Tol,ColDat,Eu_step)
    plt.plot(data.T)
    # Aggregate
    if ii == 0:
        Data_Sim   = copy.copy(data)
        Data_SimIn = [len(data[0,:])]
        Data_Param = copy.copy(NewtStps_Param)
        Data_In    = copy.copy(NewtStps_In)
        Data_Out   = copy.copy(NewtStps_Out)
    else:
        Data_Sim   = np.concatenate((Data_Sim,data),1)
        Data_SimIn = np.concatenate((Data_SimIn,[len(data[0,:])]))
        Data_Param = np.concatenate((Data_Param,NewtStps_Param),1)
        Data_In    = np.concatenate((Data_In,NewtStps_In),1)
        Data_Out   = np.concatenate((Data_Out,NewtStps_Out),1)

# Now, throw all of the data into a list, and then export as a pickle
Master_List = [Data_Sim, Data_SimIn, Data_Param, Data_In, Data_Out]
plt.show()
with open("Hopf_Data.pkl", 'wb') as f:
        pickle.dump(Master_List, f)
