import matplotlib.pyplot as plt
import torch
import pickle
import copy
import time
import cvxpy as cp
import numpy as np
import torch
import torch.nn as nn
import sys
import os

# Add functions to path
os.chdir(os.path.dirname(__file__))
sys.path.append("..\Functions")

from RK_Trap_Sim_vNN   import RKT_NN_Sim
from RK_Trap_fJ        import RK_ft, RK_Jt
from Newton_Solver     import NS
from Cubic_Oscillator  import f, J
from cvxpylayers.torch import CvxpyLayer

# Define 3 Layers: Input, Middle, and Output
class ReluInput(torch.nn.Module):
    def __init__(self, Input_size, W_size, AC, IW, W, b):
        super(ReluInput, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(Input_size, W_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((W_size, Input_size))
        I  = torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc[:,0:Input_size]-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])
        self.ReLU = nn.ReLU()
        #self.ii   = 1

    def forward(self, x):
        #self.ii = self.ii + 1
        if self.AC == 1:
            #if self.ii%1 == 0:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn.copy_(Wcn[:,0:Input_size])
            self.linear.weight = Wn
        return self.ReLU(self.linear(x)) # Apply ReLU and Return
class ReluLayer(torch.nn.Module):
    def __init__(self, W_size, AC, IW, W, b):
        super(ReluLayer, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(W_size, W_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((W_size, W_size))
        x  = cp.Parameter((W_size, 1))
        I  = torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])
        self.ReLU = nn.ReLU()
        #self.ii = 1

    def forward(self, x):
        #self.ii = self.ii + 1
        if self.AC == 1:
            #if self.ii%1 == 0:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn.copy_(Wcn)
            self.linear.weight = Wn
        return self.ReLU(self.linear(x)) # Apply ReLU and Return
class ReluOutput(torch.nn.Module):
    def __init__(self, Output_size, W_size, AC, IW, W, b):
        super(ReluOutput, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(W_size, Output_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((Output_size, W_size))
        I  = torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc[0:Output_size,:]-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])
        #self.ii = 1

    def forward(self, x):
        #self.ii = self.ii + 1
        if self.AC == 1:
            #if self.ii%1 == 0:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn.copy_(Wcn[0:Output_size,:])
            self.linear.weight = Wn
        return self.linear(x) # Linear Layer -- No ReLU!!

# Define Net Size
W_size      = 30
Output_size = 2
Input_size  = 2*Output_size

# Load NN data
AC = 0
IW = 0
W  = 0
b  = 0
net = torch.nn.Sequential(ReluInput(Input_size,W_size,AC,IW,W,b),
                          ReluLayer(W_size,AC,IW,W,b),
                          ReluLayer(W_size,AC,IW,W,b),
                          ReluLayer(W_size,AC,IW,W,b),
                          ReluOutput(Output_size,W_size,AC,IW,W,b))

#PATH = "..\Train\cdNNCubic.pkl"
PATH = "..\Train\cdNNCubic_Unconstrained.pkl"
net.load_state_dict(torch.load(PATH))
net.double()
net.eval()

W1 = net[0].linear.weight
W2 = net[1].linear.weight
W3 = net[2].linear.weight
W4 = net[3].linear.weight
W5 = net[4].linear.weight

u, s1, v = torch.svd(W1[:,0:2])
u, s2, v = torch.svd(W2)
u, s3, v = torch.svd(W3)
u, s4, v = torch.svd(W4)
u, s5, v = torch.svd(W5)
print(s1)
print(s2)
print(s3)
print(s4)
print(s5)

# Simulation Parameters
dt = 0.025
t0 = 0
tf = 10
sys = 'C'
Step_Tol = 1e-5
ColDat  = 0
Eu_step = 0
np.random.seed(3)

x0 = np.array([0.5, -0.5])
for ii in range(1):
    # Perturb initial conditions
    x0p   = copy.copy(x0)
    x0p   = x0p + 0.1*0.25*np.random.randn(2)
    x0NNp = x0p
    print(x0p)
    Xv, XvNN = RKT_NN_Sim(sys,x0p,x0NNp,dt,t0,tf,f,J,RK_ft,RK_Jt,NS,ColDat,Eu_step,Step_Tol,net,0)
    plt.plot(Xv.T,color='orange')
    plt.plot(XvNN.T,'b--')

plt.show()
