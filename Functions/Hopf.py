import numpy as np

def f(x):
    x_dot = np.array([0,
            x[0]*x[1] + x[2] - x[1]*(x[1]**2 + x[2]**2),
            x[0]*x[2] - x[1] - x[2]*(x[1]**2 + x[2]**2)])
    return x_dot

def J(x):
    Jn = np.matrix([[0, 0, 0],
                    [x[1],x[0]-3*x[1]**2-x[2]**2,1-2*x[1]*x[2]],
                    [x[2],-1-2*x[1]*x[2],x[0]-x[1]**2-3*x[2]**2]])
    return Jn
