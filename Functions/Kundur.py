import numpy as np
import copy
import matplotlib.pyplot as plt

from Kundur_Sys  import create_power_system
from Kundur_Sys  import create_system_matrices
from Kundur_Sys  import ode_right_hand_side
from Kundur_Sys  import ode_right_hand_side_solve
from Kundur_Sys  import compute_equilibrium_state

# Call system & initialize
power_system               = create_power_system()
A, B, C, D, F, G, u_0, x_0 = create_system_matrices(power_system, case='normal')
x_0e = compute_equilibrium_state(power_system, u_disturbance=None, slack_bus=None, system_case='normal')
u_vector = np.reshape(u_0, (-1, 1))
u_0e     = u_vector.flatten()

# Compute State Derivatives
dxdt = ode_right_hand_side_solve(0, x_0e, u_0, A, B, C, D, F)

# Build System Jacobian -- Example:
''' x_vector = np.reshape(x_0e, (-1, 1))
    u_vector = np.reshape(u_0, (-1, 1))
    u0       = u_vector.flatten()
    FCX      = D @ np.sin(C @ x_vector)
    dx       = A @ x_vector + F @ FCX + B @ u_vector
    J        = A + F @ D @ (np.diag(np.cos(C @ x_vector.flatten())) @ C)'''

# Let's build RHS funtion "f" and system Jacobian "J"
def f(x):
    FCX = D @ np.sin(C @ x)
    x_dot = A @ x + F @ FCX + B @ u_0e
    return x_dot.flatten()

def J(x):
    Jn = A + F @ D @ (np.diag(np.cos(C @ x.flatten())) @ C)
    return Jn

def x0f():
    return x_0e.flatten()

######### Proof: Compare to Sensitivity #########
'''dxdt = ode_right_hand_side_solve(0, x_0e, u_0, A, B, C, D, F)
epsilon = 0.001

x_0 = copy.copy(x_0e)
x_0[0] = x_0[0] + epsilon
d0 = ode_right_hand_side_solve(0, x_0, u_0, A, B, C, D, F)

x_0 = copy.copy(x_0e)
x_0[1] = x_0[1] + epsilon
d1 = ode_right_hand_side_solve(0, x_0, u_0, A, B, C, D, F)

x_0 = copy.copy(x_0e)
x_0[2] = x_0[2] + epsilon
d2 = ode_right_hand_side_solve(0, x_0, u_0, A, B, C, D, F)

x_0 = copy.copy(x_0e)
x_0[3] = x_0[3] + epsilon
d3 = ode_right_hand_side_solve(0, x_0, u_0, A, B, C, D, F)

x_0 = copy.copy(x_0e)
x_0[4] = x_0[4] + epsilon
d4 = ode_right_hand_side_solve(0, x_0, u_0, A, B, C, D, F)

x_0 = copy.copy(x_0e)
x_0[5] = x_0[5] + epsilon
d5 = ode_right_hand_side_solve(0, x_0, u_0, A, B, C, D, F)

x_0 = copy.copy(x_0e)
x_0[6] = x_0[6] + epsilon
d6 = ode_right_hand_side_solve(0, x_0, u_0, A, B, C, D, F)

x_0 = copy.copy(x_0e)
x_0[7] = x_0[7] + epsilon
d7 = ode_right_hand_side_solve(0, x_0, u_0, A, B, C, D, F)

x_0 = copy.copy(x_0e)
x_0[8] = x_0[8] + epsilon
d8 = ode_right_hand_side_solve(0, x_0, u_0, A, B, C, D, F)

x_0 = copy.copy(x_0e)
x_0[9] = x_0[9] + epsilon
d9 = ode_right_hand_side_solve(0, x_0, u_0, A, B, C, D, F)

print("--------------")
Jsi = (d9-dxdt)/epsilon
print(Jsi)
print(J[:,9])'''
