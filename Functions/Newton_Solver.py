import numpy as np

def NS(Jn,fn,x,alpha):
    xnew = x - alpha*np.linalg.solve(Jn, fn)
    return xnew
