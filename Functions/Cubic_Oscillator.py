import numpy as np

def f(x):
    x_dot = np.array([-0.1*(x[0]**3) + 2*(x[1]**3),
            -2*(x[0]**3) - 0.1*(x[1]**3)])
    return x_dot

def J(x):
    Jn = np.matrix([[-0.3*(x[0]**2), 6*(x[1]**2)],
         [-6*(x[0]**2), -0.3*(x[1]**2)]])
    return Jn
