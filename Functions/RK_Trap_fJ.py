import numpy as np

def RK_ft(dt,J,f,x1,k2):
    k1 = f(x1)
    x2 = x1 + 0.5*dt*k1 + 0.5*dt*k2
    ft = k2 - f(x2)
    return ft

def RK_Jt(dt,J,f,x1,k2,n):
    k1 = f(x1)
    x2 = x1 + 0.5*dt*k1 + 0.5*dt*k2
    Jt = np.identity(n)-0.5*dt*J(k2)
    return Jt
