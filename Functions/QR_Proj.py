import numpy as np
import torch
import cvxpy as cp

def QRProj(NNData,W,Mat_Loc,EVR): # 1 = Input, 2 = Middle, 3 = Output
    if Mat_Loc == 1:
        In_size  = len(NNData)
        W_size   = len(W)
        dat_size = len(NNData.T)
        A  = torch.zeros((W_size*dat_size,In_size*W_size))
        B  = W@NNData
        b  = B.flatten()
        for ii in range(W_size):
            i1 = 0        + ii*dat_size
            i2 = dat_size + ii*dat_size
            i3 = 0        + ii*In_size
            i4 = In_size  + ii*In_size
            A[i1:i2,i3:i4] = NNData.T

        # QR Formulation
        Q, R = torch.qr(A)
        bu = (Q.T@b).detach().numpy()
        MM = R.detach().numpy()
        xm = cp.Variable((W_size, W_size))
        xv = cp.vec(xm[:,0:In_size].T)
        I  = EVR*torch.eye(W_size)
        M1 = cp.vstack((I, xm.T))
        M2 = cp.vstack((xm,I))
        M  = cp.hstack((M1,M2))

        problem = cp.Problem(cp.Minimize(cp.sum_squares(MM@xv-bu)),[M>>0])
        problem.solve(solver=cp.SCS,max_iters=25000)
        Wcm = xm.value
        Wc  = Wcm[:,0:In_size]

    elif Mat_Loc == 2:
        W_size   = len(W)
        dat_size = len(NNData.T)
        A  = torch.zeros((W_size*dat_size,W_size*W_size))
        B  = W@NNData
        b  = B.flatten()
        for ii in range(W_size):
            i1 = 0        + ii*dat_size
            i2 = dat_size + ii*dat_size
            i3 = 0        + ii*W_size
            i4 = W_size   + ii*W_size
            A[i1:i2,i3:i4] = NNData.T

        # SVD Formulation
            #u, s1, v = torch.svd_lowrank(A,q=1)
            #u, s1, v = torch.svd(A,some=True)
            #bu       = u.T@b
            #MM       = S@v.T
            #S        = torch.diag(s1)

        # QR Formulation
        Q, R = torch.qr(A)
        bu = (Q.T@b).detach().numpy()
        MM = R.detach().numpy()
        xm = cp.Variable((W_size, W_size))
        xv = cp.vec(xm.T)
        I  = EVR*torch.eye(W_size)
        M1 = cp.vstack((I, xm.T))
        M2 = cp.vstack((xm,I))
        M  = cp.hstack((M1,M2))
        problem = cp.Problem(cp.Minimize(cp.sum_squares(MM@xv-bu)),[M>>0])
        problem.solve(solver=cp.SCS,max_iters=25000)
        Wc = xm.value
    else:
        Out_size = len(W)
        W_size   = len(W.T)
        dat_size = len(NNData.T)
        A  = torch.zeros((Out_size*dat_size,Out_size*W_size))
        B  = W@NNData
        b  = B.flatten()
        for ii in range(Out_size):
            i1 = 0        + ii*dat_size
            i2 = dat_size + ii*dat_size
            i3 = 0        + ii*W_size
            i4 = W_size   + ii*W_size
            A[i1:i2,i3:i4] = NNData.T

        # QR Formulation
        Q, R = torch.qr(A)
        bu = (Q.T@b).detach().numpy()
        MM = R.detach().numpy()
        xm = cp.Variable((W_size, W_size))
        xv = cp.vec(xm[0:Out_size,:].T)
        I  = EVR*torch.eye(W_size)
        M1 = cp.vstack((I, xm.T))
        M2 = cp.vstack((xm,I))
        M  = cp.hstack((M1,M2))

        problem = cp.Problem(cp.Minimize(cp.sum_squares(MM@xv-bu)),[M>>0])
        problem.solve(solver=cp.SCS,max_iters=25000)
        Wcm = xm.value
        Wc  = Wcm[0:Out_size,:]
    return Wc
