import numpy as np
import copy
import torch
import time

def RKT_NN_Sim(sys,x0,x0NN,dt,t0,tf,f,J,RK_ft,RK_Jt,NS,Collect_Data,Eu_step,Step_Tol,net,x0_nws):
    tspan   = np.arange(t0,tf,dt)
    k0      = f(x0)
    n       = len(x0)
    Xv      = np.empty([n,len(tspan)+1])
    Xv[:,0] = x0
    ii      = 0  # count time steps
    io      = 0  # count (time steps) x (Newton steps)

    # NN data structures (x0NN might be scaled differently than x0)
    XvNN      = np.empty([n,len(tspan)+1])
    XvNN[:,0] = x0NN
    ioNN      = 0

    # Define a structure for saving Newton Data
    if Collect_Data == 1:
        # This collects the system state (e.g., parameterization)
        red = 25 # REDundancy factor

        # This collects Newton inputs
        NewtStps_In    = np.empty([n,red*(len(tspan)+1)])
        NewtStps_InNN  = np.empty([n,red*(len(tspan)+1)])

        # This collects Newton ouputs
        NewtStps_Out   = np.empty([n,red*(len(tspan)+1)])
        NewtStps_OutNN = np.empty([n,red*(len(tspan)+1)])

    for t in tspan:
        # Solve Newton for this time step
        x1   = Xv[:,ii]
        k1   = f(x1)
        x1NN = XvNN[:,ii]
        if sys == 'K':
            x1NN[4:8] = x1NN[4:8] + x0[5] # this will always be omega_0
            k1NN      = f(x1NN)
            x1NN[4:8] = x1NN[4:8] - x0[5]
        else:
            k1NN = f(x1NN)

        k1NN = k1
        if Eu_step == 1:
            # Initial Guess: Euler
            k2   = f(x1  +dt*k1)
            k2NN = f(x1NN+dt*k1NN)
        else:
            # Cold start: useful for training
            k2   = k1
            k2NN = k1NN

        ###################################
        ############## NEWTON #############
        ###################################
        jj = 0           # count Newton Steps
        k20 = 100*copy.copy(k2)
        # If you want to use Newton with functional convergence, use below
        # while abs(max(RK_ft(dt,J,f,x1,k2), key=abs)) > Newt_Tol:
        while abs(max(k20-k2, key=abs)) > Step_Tol:
            # Define Jacobian and Function Values
            fn = RK_ft(dt,J,f,x1,k2)
            Jn = RK_Jt(dt,J,f,x1,k2,n)
            alpha = 1;
            k20 = copy.copy(k2)
            k2  = NS(Jn,fn,k2,alpha)
            jj += 1
            io += 1
            if Collect_Data == 1:
                NewtStps_In[:,io]     = k20
                NewtStps_Out[:,io]    = k2

        ###################################
        ############## CoNNS ##############
        ###################################
        jjNN  = 0
        k20NN = 100*copy.copy(k2NN)
        while abs(max(k20NN-k2NN, key=abs)) > Step_Tol:
            # Feed Data into the NN -- before doing so, subtract out the ICs
            if sys == 'K':
                x1NN_0m = copy.copy(x1NN)
                x1NN_0m = x1NN_0m - x0_nws
                NNData_In = torch.from_numpy(np.concatenate((k2NN,x1NN_0m),0).transpose())
            else:
                NNData_In = torch.from_numpy(np.concatenate((k2NN,x1NN),0).transpose())

            k20NN     = copy.copy(k2NN)
            k2NN      = net(NNData_In).detach().numpy()
            jjNN     += 1
            ioNN     += 1
            if Collect_Data == 1:
                NewtStps_InNN[:,ioNN]  = k20NN
                NewtStps_OutNN[:,ioNN] = k2NN
        print(jj)
        print(jjNN)
        print("-------")

        # Take 1 Newton step :)
        #fn   = RK_ft(dt,J,f,x1NN,k2NN)
        #Jn   = RK_Jt(dt,J,f,x1NN,k2NN,n)
        #k2NN = NS(Jn,fn,k2NN,alpha)

        # Now, compute new state & concatenate
        ii  += 1
        x2   = x1   + 0.5*dt*k1   + 0.5*dt*k2
        x2NN = x1NN + 0.5*dt*k1NN + 0.5*dt*k2NN
        Xv[:,ii]   = x2
        XvNN[:,ii] = x2NN

    if Collect_Data == 1:
        print(io)
        print(ioNN)

        # Remove zero columns from vectors
        NewtStps_In    = np.delete(NewtStps_In, np.arange(io,red*(len(tspan)+1)), 1)
        NewtStps_Out   = np.delete(NewtStps_Out, np.arange(io,red*(len(tspan)+1)), 1)

        # Same for NN
        NewtStps_InNN  = np.delete(NewtStps_InNN, np.arange(ioNN,red*(len(tspan)+1)), 1)
        NewtStps_OutNN = np.delete(NewtStps_OutNN, np.arange(ioNN,red*(len(tspan)+1)), 1)

        return Xv, XvNN, NewtStps_In, NewtStps_InNN, NewtStps_Out, NewtStps_OutNN

    else:
        print(io)
        print(ioNN)
        return Xv, XvNN
