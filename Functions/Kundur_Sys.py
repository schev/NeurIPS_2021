import numpy as np
from scipy.optimize import fsolve, root

def create_power_system():
    n_buses = 6
    n_generators = 4
    n_non_generators = n_buses - n_generators
    n_states = 2 * n_generators + 1 * n_non_generators

    omega_0 = 2 * np.pi * 60

    output_scaling = np.ones((n_states, 1))
    output_scaling[n_generators:2 * n_generators] = omega_0

    output_offset = np.zeros((n_states, 1))
    output_offset[n_generators:2 * n_generators] = -omega_0

    H_generators = np.array([58.5, 58.5, 55.575, 55.575])
    D_generators = 0.0 * np.ones(n_generators)
    K_g_generators = 0.5 * np.ones(n_generators)
    T_m_generators = np.array([0.1, 0.2, 0.05, 0.1])
    T_g_generators = 0.01 * np.ones(n_generators)

    # D_non_generators = np.array([0.02565, 0.04687])
    D_non_generators = np.array([0.1, 0.2]) * 2

    P_load_set_point = np.array([-9.67, -17.67])
    P_generator_set_point = np.array([7, 7, 6.34, 7])
    P_set_point = np.hstack([P_generator_set_point, P_load_set_point])

    P_disturbance = np.zeros(n_buses)
    slack_bus_idx = 2
    #
    V_magnitude = np.array([1.0300,
                            1.0100,
                            1.0300,
                            1.0100,
                            0.9610,
                            0.9714])

    # short circuit at bus 9
    V_magnitude_short_circuit = np.array([1.0300,
                                          1.0100,
                                          1.0300,
                                          1.0100,
                                          0.9610,
                                          0.000])

    B_susceptance = np.array([7.8461,
                              7.8461,
                              12.9499,
                              32.5581,
                              12.9499,
                              32.5581,
                              9.0982])

    # trip one line between bus 10 and 11 (line index 10), susceptance halfed
    B_susceptance_line_tripped = np.array([7.8461,
                                           7.8461,
                                           12.9499,
                                           32.5581,
                                           12.9499,
                                           32.5581,
                                           6.0655])

    b_from = np.array([0,
                       2,
                       0,
                       1,
                       2,
                       3,
                       4], dtype=int)

    b_to = np.array([1,
                     3,
                     4,
                     4,
                     5,
                     5,
                     5], dtype=int)

    V_i_V_j_B_full = V_magnitude[b_from] * V_magnitude[b_to] * B_susceptance
    V_i_V_j_B_short_circuit = V_magnitude_short_circuit[b_from] * V_magnitude_short_circuit[b_to] * B_susceptance
    V_i_V_j_B_line_tripped = V_magnitude[b_from] * V_magnitude[b_to] * B_susceptance_line_tripped

    incidence_matrix = np.array([[1, -1, 0, 0, 0, 0],
                                 [0, 0, 1, -1, 0, 0],
                                 [1, 0, 0, 0, -1, 0],
                                 [0, 1, 0, 0, -1, 0],
                                 [0, 0, 1, 0, 0, -1],
                                 [0, 0, 0, 1, 0, -1],
                                 [0, 0, 0, 0, 1, -1]])

    t_max = 2.0

    system_parameters = {'n_buses': n_buses,
                         'n_generators': n_generators,
                         'n_non_generators': n_non_generators,
                         'n_states': n_states,
                         'slack_bus_idx': slack_bus_idx,
                         'H_generators': H_generators,
                         'D_generators': D_generators,
                         'omega_0': omega_0,
                         'output_scaling': output_scaling,
                         'T_m_generators': T_m_generators,
                         'T_g_generators': T_g_generators,
                         'K_g_generators': K_g_generators,
                         'D_non_generators': D_non_generators,
                         'P_disturbance': P_disturbance,
                         'P_set_point': P_set_point,
                         'V_i_V_j_B_full': V_i_V_j_B_full,
                         'V_i_V_j_B_short_circuit': V_i_V_j_B_short_circuit,
                         'V_i_V_j_B_line_tripped': V_i_V_j_B_line_tripped,
                         'incidence_matrix': incidence_matrix,
                         't_max': t_max,
                         'output_offset': output_offset}

    print('Successfully created the reduced Kundur 2 area system (6 buses, 4 generators)!')

    return system_parameters


def create_system_matrices(power_system, case='normal'):
    # model with two states plus a governor
    n_g = power_system['n_generators']
    n_b = power_system['n_buses']
    n_d = n_b - n_g
    H_total = sum(power_system['H_generators'])

    # --------------------------------
    # A-matrix
    A_11 = np.zeros((n_g, n_g))
    A_12 = (np.eye(n_g) * H_total - np.repeat(power_system['H_generators'].reshape((1, n_g)), repeats=n_g,
                                              axis=0)) / H_total
    A_21 = np.zeros((n_g, n_g))
    A_22 = np.diag(-power_system['omega_0'] / (2 * power_system['H_generators']) * (
            power_system['D_generators'] + power_system['K_g_generators']))

    A_13 = np.zeros((n_g, n_d))
    A_23 = np.zeros((n_g, n_d))
    A_31 = np.zeros((n_d, n_g))
    A_32 = np.zeros((n_d, n_g))
    A_33 = np.zeros((n_d, n_d))

    A = np.block([
        [A_11, A_12, A_13],
        [A_21, A_22, A_23],
        [A_31, A_32, A_33]
    ])

    # --------------------------------
    # F-matrix
    F_11 = np.zeros((n_g, n_g))
    F_21 = np.diag(-power_system['omega_0'] / (2 * power_system['H_generators']))

    F_12 = np.zeros((n_g, n_d))
    F_22 = np.zeros((n_g, n_d))
    F_31 = np.zeros((n_d, n_g))
    F_32 = np.diag(-1 / power_system['D_non_generators'])

    F = np.block([
        [F_11, F_12],
        [F_21, F_22],
        [F_31, F_32]
    ])

    # --------------------------------
    # B-matrix
    # B_11 = -np.ones((n_g, 1))
    B_11 = np.zeros((n_g, 1))
    B_12 = np.zeros((n_g, n_g))
    B_21 = np.reshape(power_system['omega_0'] / (2 * power_system['H_generators']) * power_system[
        'K_g_generators'], (n_g, 1))
    B_22 = np.diag(power_system['omega_0'] / (2 * power_system['H_generators']))

    B_13 = np.zeros((n_g, n_d))
    B_23 = np.zeros((n_g, n_d))
    B_31 = np.zeros((n_d, 1))
    B_32 = np.zeros((n_d, n_g))
    B_33 = np.diag(1 / power_system['D_non_generators'])

    B = np.block([
        [B_11, B_12, B_13],
        [B_21, B_22, B_23],
        [B_31, B_32, B_33]
    ])

    # --------------------------------
    # U-matrix
    U_11 = np.eye(n_g)
    U_12 = np.zeros((n_g, n_g))
    U_13 = np.zeros((n_g, n_d))

    U_21 = np.zeros((n_d, n_g))
    U_22 = np.zeros((n_d, n_g))
    U_23 = np.eye(n_d)

    U = np.block([
        [U_11, U_12, U_13],
        [U_21, U_22, U_23]
    ])

    C = power_system['incidence_matrix'] @ U

    if case == 'normal':
        D = power_system['incidence_matrix'].T @ np.diag(power_system['V_i_V_j_B_full'])
    elif case == 'short_circuit':
        D = power_system['incidence_matrix'].T @ np.diag(power_system['V_i_V_j_B_short_circuit'])
    elif case == 'line_tripped':
        D = power_system['incidence_matrix'].T @ np.diag(power_system['V_i_V_j_B_line_tripped'])
    else:
        raise Exception('Specify a valid case')

    # adjustment of u to accommodate power disturbance input
    G = np.block([
        [np.zeros((1, n_b))],
        [np.eye(n_b)]
    ])

    # set point of the power before any disturbance
    u_0 = np.hstack([power_system['omega_0'],
                     power_system['P_set_point'][:n_g] + power_system['D_generators'] * power_system['omega_0'],
                     power_system['P_set_point'][n_g:]]).reshape((-1, 1))

    # initial value for equilibrium computation
    x_0 = np.hstack([np.zeros(n_g),
                     np.ones(n_g) * power_system['omega_0'],
                     np.zeros(n_d)]).reshape((-1, 1))

    return A, B, C, D, F, G, u_0, x_0

def ode_right_hand_side_solve(t, x, u, A, B, C, D, F):
    x_vector = np.reshape(x, (-1, 1))
    u_vector = np.reshape(u, (-1, 1))

    FCX = D @ np.sin(C @ x_vector)

    dx = A @ x_vector + F @ FCX + B @ u_vector
    return dx[:, 0]


def compute_equilibrium_state(power_system, u_disturbance=None, slack_bus=None, system_case='normal'):
    A, B, C, D, F, G, u_0, x_0 = create_system_matrices(power_system=power_system, case=system_case)

    if u_disturbance is not None:
        u = u_0 + u_disturbance
    else:
        u = u_0

    if system_case == 'short_circuit':
        raise Exception('No equilibrium will be found for short circuit configurations.')

    x_equilibrium, info_dict, ier, mesg = fsolve(ode_right_hand_side,
                                                 x0=x_0,
                                                 args=(u, A, B, C, D, F, slack_bus),
                                                 xtol=1.49012e-08,
                                                 full_output=True)

    if not np.allclose(info_dict['fvec'],
                       np.zeros(info_dict['fvec'].shape),
                       atol=1e-08):
        raise Exception(f'No equilibrium found. Error message {mesg}')
    else:
        return x_equilibrium.reshape((-1, 1))


def ode_right_hand_side(x, u, A, B, C, D, F, slack=None):
    x_vector = np.reshape(x, (-1, 1))
    if slack is not None:
        x_vector[slack] = 0

    FCX = D @ np.sin(C @ x_vector)

    dx = A @ x_vector + F @ FCX + B @ u
    return dx[:, 0]


def separate_states(x_states, n_generators=4, n_states=15):
    if x_states.shape[1] is not n_states:
        raise Exception('Please transform the data such that they can be split along the second axis.')
    x_delta_generators, x_omega, x_delta_non_generators = np.split(x_states, [n_generators, 2 * n_generators], axis=1)
    x_delta = np.concatenate([x_delta_generators, x_delta_non_generators], axis=1)

    return x_delta, x_omega


def combine_states(x_delta, x_omega, n_generators=4, n_states=15):
    if x_delta.shape[1] + x_omega.shape[1] is not n_states:
        raise Exception('Please transform the data such that they can be concatenated along the second axis.')

    x_delta_generators, x_delta_non_generators = np.split(x_delta, [n_generators], axis=1)
    x_states = np.concatenate([x_delta_generators,
                               x_omega,
                               x_delta_non_generators], axis=1)
    return x_states


def input_data_initialised(n_ops, power_system):
    time_zeros = np.zeros((n_ops, 1))
    power_zeros = np.zeros((n_ops, power_system['n_buses']))
    delta_initial = np.zeros((n_ops, power_system['n_buses']))
    omega_initial = np.zeros((n_ops, power_system['n_generators']))

    delta_results_zeros = np.zeros((n_ops, power_system['n_buses']))
    omega_results_zeros = np.zeros((n_ops, power_system['n_generators']))
    delta_t_results_zeros = np.zeros((n_ops, power_system['n_buses']))
    omega_t_results_zeros = np.zeros((n_ops, power_system['n_generators']))
    data_type_zeros = np.zeros((n_ops,
                                1 * power_system['n_generators'] + power_system['n_buses'],
                                2))

    data_initialised = {'time': time_zeros,
                        'power': power_zeros,
                        'delta_initial': delta_initial,
                        'omega_initial': omega_initial,
                        'delta_results': delta_results_zeros,
                        'omega_results': omega_results_zeros,
                        'delta_t_results': delta_t_results_zeros,
                        'omega_t_results': omega_t_results_zeros,
                        'data_type': data_type_zeros}

    return data_initialised


def create_training_data(n_time_steps, n_power_steps, t_short_circuit, bus_disturbance, power_system):
    n_ops = n_time_steps * n_power_steps
    data_ops = input_data_initialised(n_ops=n_power_steps,
                                      power_system=power_system)
    power_ops = np.zeros((n_power_steps, power_system['n_buses']))
    power_ops[:, bus_disturbance] = np.linspace(0, 5, n_power_steps)
    data_ops.update(time=np.ones((n_power_steps, 1)) * 10,
                    power=power_ops)
    data_type = data_ops['data_type']
    data_type[:, :, 0] = np.ones((n_power_steps, power_system['n_states']))

    x_equilibrium_undisturbed = compute_equilibrium_state(power_system,
                                                          u_disturbance=None,
                                                          slack_bus=power_system['slack_bus_idx'],
                                                          system_case='normal')

    delta_initial_undisturbed, omega_initial_undisturbed = separate_states(x_equilibrium_undisturbed.T,
                                                                           n_states=power_system['n_states'])

    data_ops.update(delta_initial=np.repeat(delta_initial_undisturbed, repeats=n_power_steps, axis=0),
                    omega_initial=np.repeat(omega_initial_undisturbed, repeats=n_power_steps, axis=0),
                    data_type=data_type)

    data_ops = evaluate_ops(data_ops, 'normal', power_system)

    data_ops.update(delta_initial=data_ops['delta_results'],
                    omega_initial=data_ops['omega_results'],
                    time=np.ones((n_power_steps, 1)) * t_short_circuit)

    data_ops = evaluate_ops(data_ops, 'short_circuit', power_system)

    shorted_bus_angles = data_ops['delta_results'][:, 5:6]
    shorted_bus_angle_offset = np.floor((shorted_bus_angles + np.pi) / (2 * np.pi)) * 2 * np.pi
    delta_results = data_ops['delta_results']
    delta_results[:, 5:6] = delta_results[:, 5:6] - shorted_bus_angle_offset

    t_max = power_system['t_max']
    data_ops.update(delta_initial=delta_results,
                    omega_initial=data_ops['omega_results'],
                    time=np.ones((n_power_steps, 1)) * t_max)

    data_ops = evaluate_op_trajectory(data_ops, n_time_steps=n_time_steps,
                                      system_case='line_tripped', power_system=power_system)

    data_ops = calculate_data_ode_right_hand_side(data_ops, 'line_tripped', power_system)

    return data_ops


def create_RK_training_data(n_power_steps, t_short_circuit, bus_disturbance, power_system):
    data_ops = input_data_initialised(n_ops=n_power_steps,
                                      power_system=power_system)
    power_ops = np.zeros((n_power_steps, power_system['n_buses']))
    power_ops[:, bus_disturbance] = np.linspace(0, 5, n_power_steps)
    data_ops.update(time=np.ones((n_power_steps, 1)) * 10,
                    power=power_ops)
    data_type = data_ops['data_type']
    data_type[:, :, 0] = np.ones((n_power_steps, power_system['n_states']))

    x_equilibrium_undisturbed = compute_equilibrium_state(power_system,
                                                          u_disturbance=None,
                                                          slack_bus=power_system['slack_bus_idx'],
                                                          system_case='normal')

    delta_initial_undisturbed, omega_initial_undisturbed = separate_states(x_equilibrium_undisturbed.T,
                                                                           n_states=power_system['n_states'])

    data_ops.update(delta_initial=np.repeat(delta_initial_undisturbed, repeats=n_power_steps, axis=0),
                    omega_initial=np.repeat(omega_initial_undisturbed, repeats=n_power_steps, axis=0),
                    data_type=data_type)

    data_ops = evaluate_ops(data_ops, 'normal', power_system)

    data_ops.update(delta_initial=data_ops['delta_results'],
                    omega_initial=data_ops['omega_results'],
                    time=np.ones((n_power_steps, 1)) * t_short_circuit)

    data_ops = evaluate_ops(data_ops, 'short_circuit', power_system)

    shorted_bus_angles = data_ops['delta_results'][:, 5:6]
    shorted_bus_angle_offset = np.floor((shorted_bus_angles + np.pi) / (2 * np.pi)) * 2 * np.pi
    delta_results = data_ops['delta_results']
    delta_results[:, 5:6] = delta_results[:, 5:6] - shorted_bus_angle_offset

    data_ops.update(delta_results=delta_results,
                    omega_results=data_ops['omega_results'])

    data_ops = calculate_data_ode_right_hand_side(data_ops, 'short_circuit', power_system)

    return data_ops


def calculate_data_ode_right_hand_side(data_ops, system_case, power_system):
    states_results = combine_states(data_ops['delta_results'], data_ops['omega_results'], n_states=power_system[
        'n_states'])
    A, B, C, D, F, G, u_0, x_0 = create_system_matrices(power_system=power_system, case=system_case)

    u_disturbance = data_ops['power'] @ G.T
    u = u_0.T + u_disturbance

    solver_func = functools.partial(ode_right_hand_side_solve, A=A, B=B, C=C, D=D, F=F)

    solver_results = map(solver_func,
                         data_ops['time'],
                         states_results,
                         u)

    list_solver_results = list(solver_results)

    state_t_results = np.concatenate([single_solver_result.reshape((1, -1)) for single_solver_result in
                                      list_solver_results],
                                     axis=0)

    delta_t_results, omega_t_results = separate_states(state_t_results, n_states=power_system['n_states'])

    data_ops.update(delta_t_results=delta_t_results,
                    omega_t_results=omega_t_results)

    return data_ops


def create_collocation_points(n_time_steps, n_power_steps, bus_disturbance, power_system):
    n_ops = n_time_steps * n_power_steps
    data_ops = input_data_initialised(n_ops=n_ops,
                                      power_system=power_system)
    power_ops = np.zeros((n_power_steps, power_system['n_buses']))
    power_ops[:, bus_disturbance] = np.linspace(0, 5, n_power_steps)
    time_steps = np.repeat(np.linspace(start=0, stop=power_system['t_max'], num=n_time_steps).reshape((1, -1)),
                           repeats=n_power_steps, axis=0).reshape((-1, 1))

    data_ops.update(power=np.repeat(power_ops, repeats=n_time_steps, axis=0),
                    time=time_steps)

    return data_ops


def evaluate_ops(data_ops, system_case, power_system):
    states_initial = combine_states(data_ops['delta_initial'], data_ops['omega_initial'], n_states=power_system[
        'n_states'])
    t_span = np.concatenate([data_ops['time'] * 0,
                             data_ops['time']], axis=1)

    A, B, C, D, F, G, u_0, x_0 = create_system_matrices(power_system=power_system, case=system_case)

    u_disturbance = data_ops['power'] @ G.T
    u = u_0.T + u_disturbance
    solver_func = functools.partial(solve_ode, A=A, B=B, C=C, D=D, F=F)

    solver_results = map(solver_func,
                         t_span,
                         data_ops['time'],
                         states_initial,
                         u)

    list_solver_results = list(solver_results)

    state_results = np.concatenate([single_solver_result.T for single_solver_result in list_solver_results], axis=0)

    delta_results, omega_results = separate_states(state_results, n_states=power_system['n_states'])

    data_ops.update(delta_results=delta_results,
                    omega_results=omega_results)

    return data_ops


def solve_ode(t_span,
              t_eval,
              states_initial,
              u, A, B, C, D, F):
    ode_solution = integrate.solve_ivp(ode_right_hand_side_solve,
                                       t_span=t_span,
                                       y0=states_initial.flatten(),
                                       args=[u, A, B, C, D, F],
                                       t_eval=t_eval,
                                       rtol=1e-5)

    return ode_solution.y


def evaluate_op_trajectory(data_ops, n_time_steps, system_case, power_system):
    n_ops = data_ops['time'].shape[0]
    t_max = power_system['t_max']

    t_span = np.concatenate([np.zeros(data_ops['time'].shape),
                             np.ones(data_ops['time'].shape) * t_max], axis=1)
    t_eval_vector = np.linspace(start=0, stop=t_max, num=n_time_steps).reshape((1, -1))
    t_eval = np.repeat(t_eval_vector, repeats=n_ops, axis=0)

    states_initial = combine_states(data_ops['delta_initial'], data_ops['omega_initial'],
                                    n_states=power_system['n_states'])
    A, B, C, D, F, G, u_0, x_0 = create_system_matrices(power_system=power_system, case=system_case)

    u_disturbance = data_ops['power'] @ G.T
    u = u_0.T + u_disturbance

    solver_func = functools.partial(solve_ode, A=A, B=B, C=C, D=D, F=F)

    solver_results = map(solver_func,
                         t_span,
                         t_eval,
                         states_initial,
                         u)

    list_solver_results = list(solver_results)

    state_results = np.concatenate([single_solver_result.T for single_solver_result in list_solver_results], axis=0)

    delta_results, omega_results = separate_states(state_results, n_states=power_system['n_states'])

    data_ops.update(time=t_eval.flatten().reshape((-1, 1)),
                    power=np.repeat(data_ops['power'], repeats=n_time_steps, axis=0),
                    delta_initial=np.repeat(data_ops['delta_initial'], repeats=n_time_steps, axis=0),
                    omega_initial=np.repeat(data_ops['omega_initial'], repeats=n_time_steps, axis=0),
                    delta_results=delta_results,
                    omega_results=omega_results,
                    data_type=np.repeat(data_ops['data_type'], repeats=n_time_steps, axis=0))

    return data_ops


def concatenate_data(data_set_list):
    data_ops_combined = {}
    for key in data_set_list[0]:
        data_ops_combined[key] = np.concatenate([single_data_set[key] for single_data_set in data_set_list], axis=0)

    return data_ops_combined


if __name__ == '__main__':
    from system_creation.power_system_handling import load_power_system

    n_time_steps = 201
    n_power_steps = 11
    t_short_circuit = 0.05
    bus_disturbance = 4
    power_system = load_power_system(6)
    t_max = 1.6
    power_system['t_max'] = t_max

    data = create_training_data(n_time_steps, n_power_steps, t_short_circuit, bus_disturbance, power_system)
