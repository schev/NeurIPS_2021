import numpy as np

def f(x):
    x_dot = np.array([10*(x[1]-x[0]),
            x[0]*(28-x[2]) - x[1],
            x[0]*x[1]-(8/3)*x[2]])
    return x_dot

def J(x):
    Jn = np.matrix([[-10, 10, 0],
                    [28-x[2], -1, -x[0]],
                    [x[1], x[0], -(8/3)]])
    return Jn
