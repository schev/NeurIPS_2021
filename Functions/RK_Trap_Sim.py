import numpy as np
import copy

def RKT_Sim(x0,dt,t0,tf,f,J,RK_ft,RK_Jt,NS,Newt_Tol,Collect_Data,Eu_step):
    tspan   = np.arange(t0,tf,dt)
    k0      = f(x0)
    n       = len(x0)
    Xv      = np.empty([n,len(tspan)+1])
    Xv[:,0] = x0
    ii      = 0  # count time steps
    io      = 0  # count (time steps) x (Newton steps)

    # Define a structure for saving Newton Data
    if Collect_Data == 1:
        # This collects the system state (e.g., parameterization)
        red = 25 # REDundancy factor
        NewtStps_Param = np.empty([n,red*(len(tspan)+1)])
        # This collects Newton inputs
        NewtStps_In    = np.empty([n,red*(len(tspan)+1)])
        # This collects Newton ouputs
        NewtStps_Out   = np.empty([n,red*(len(tspan)+1)])

    for t in tspan:
        # Solve Newton for this time step
        x1 = Xv[:,ii]
        k1 = f(x1)
        if Eu_step == 1:
            # Initial Guess: Euler
            k2 = f(x1+dt*k1)
        else:
            # Cold start: useful for training
            k2 = k1

        jj = 0           # count Newton Steps
        while abs(max(RK_ft(dt,J,f,x1,k2), key=abs)) > Newt_Tol:
            # Define Jacobian and Function Values
            fn = RK_ft(dt,J,f,x1,k2)
            Jn = RK_Jt(dt,J,f,x1,k2,n)
            alpha = 1;
            k20 = copy.copy(k2)
            k2 = NS(Jn,fn,k2,alpha)
            jj += 1
            if Collect_Data == 1:
                NewtStps_Param[:,io]  = Xv[:,ii]
                NewtStps_In[:,io]     = k20
                NewtStps_Out[:,io]    = k2
                io += 1

        # How many Newton Steps were taken?
        if t<0.01:
            print(jj) # --------------------------------------

        # Now, compute new state & concatenate
        ii += 1
        x2 = x1 + 0.5*dt*k1 + 0.5*dt*k2
        Xv[:,ii] = x2

    if Collect_Data == 1:
        print(io)

        # Remove zero columns from vectors
        NewtStps_Param = np.delete(NewtStps_Param, np.arange(io,red*(len(tspan)+1)), 1)
        NewtStps_In    = np.delete(NewtStps_In, np.arange(io,red*(len(tspan)+1)), 1)
        NewtStps_Out   = np.delete(NewtStps_Out, np.arange(io,red*(len(tspan)+1)), 1)

        return Xv, NewtStps_Param, NewtStps_In, NewtStps_Out

    else:
        return Xv
