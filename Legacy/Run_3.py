from cvxpylayers.torch import CvxpyLayer
import matplotlib.pyplot as plt
import torch
import pickle
import copy
import time
import cvxpy as cp
import numpy as np
import torch.nn as nn

from SVD_Proj    import SvdProj

# Ensure LAPACK and BLAS are included (Only in Anaconda...)
# # # # np.show_config()

# Define 3 Layers: Input, Middle, and Output
class ReluInput(torch.nn.Module):
    def __init__(self, Input_size, W_size, AC, IW, W, b, EVR):
        super(ReluInput, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(2*Input_size, W_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((W_size, Input_size))
        I  = EVR*torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc[:,0:Input_size]-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])
        self.ReLU = nn.ReLU()
        #self.ii   = 1

    def forward(self, x):
        #self.ii = self.ii + 1
        if self.AC == 1:
            #if self.ii%1 == 0:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn[:,0:Input_size])[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn[:,0:Input_size].copy_(Wcn[:,0:Input_size])
            self.linear.weight = Wn
        return self.ReLU(self.linear(x)) # Apply ReLU and Return
class ReluLayer(torch.nn.Module):
    def __init__(self, W_size, AC, IW, W, b, EVR):
        super(ReluLayer, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(W_size, W_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((W_size, W_size))
        x  = cp.Parameter((W_size, 1))
        I  = EVR*torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])
        self.ReLU = nn.ReLU()
        #self.ii = 1

    def forward(self, x):
        #self.ii = self.ii + 1
        if self.AC == 1:
            #if self.ii%1 == 0:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn.copy_(Wcn)
            self.linear.weight = Wn
        return self.ReLU(self.linear(x)) # Apply ReLU and Return
class ReluOutput(torch.nn.Module):
    def __init__(self, Output_size, W_size, AC, IW, W, b, EVR):
        super(ReluOutput, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(W_size, Output_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((Output_size, W_size))
        I  = EVR*torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc[0:Output_size,:]-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])
        #self.ii = 1

    def forward(self, x):
        #self.ii = self.ii + 1
        if self.AC == 1:
            #if self.ii%1 == 0:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn.copy_(Wcn[0:Output_size,:])
            self.linear.weight = Wn
        return self.linear(x) # Linear Layer -- No ReLU!!

# Define Net Size
W_size      = 25
Output_size = 2
Input_size  = 2 # Do not treat the parameters as inputs!

# Define Network
AC = IW = W = b = 0
EVR = 0.999
net = torch.nn.Sequential(ReluInput(Input_size,W_size,AC,IW,W,b,EVR),
                          ReluLayer(W_size,AC,IW,W,b,EVR),
                          ReluLayer(W_size,AC,IW,W,b,EVR),
                          ReluOutput(Output_size,W_size,AC,IW,W,b,EVR))

# Load and parse pickle data
with open("Cubic_Data.pkl", 'rb') as f:
    training_data = pickle.load(f)
Data_Sim   = training_data[0]
Data_SimIn = training_data[1]
Data_Param = training_data[2]
Data_In    = training_data[3]
Data_Out   = training_data[4]

# Concatenate, transpose, and prepare the data
NNData_In  = torch.from_numpy(np.concatenate((Data_In,Data_Param),0).transpose()).float()
NNData_Out = torch.from_numpy(Data_Out.transpose()).float()

# Load Network or Train from scratch
Load_Network = 1
if Load_Network == 1:
    # Define Network
    AC  = 1
    EVR = 0.999
    net = torch.nn.Sequential(ReluInput(Input_size,W_size,AC,IW,W,b,EVR),
                              ReluLayer(W_size,AC,IW,W,b,EVR),
                              ReluLayer(W_size,AC,IW,W,b,EVR),
                              ReluOutput(Output_size,W_size,AC,IW,W,b,EVR))
    PATH = "cdNN.pkl"
    net.load_state_dict(torch.load(PATH))
else:
    # Print Parameters
    '''for param in net.parameters():
          print(param.data)'''

    # Optimize (Unconstrained)
    opt = torch.optim.Adam(net.parameters(), lr=5e-3)
    for kk in range(30):
        opt.zero_grad()
        l = torch.nn.MSELoss()(net(NNData_In), NNData_Out)
        print(kk)
        print (l.item())
        l.backward()
        opt.step()

    print("-----------")

    # RE-Define Network
    W1 = net[0].linear.weight
    W2 = net[1].linear.weight
    W3 = net[2].linear.weight
    W4 = net[3].linear.weight
    b1 = net[0].linear.bias
    b2 = net[1].linear.bias
    b3 = net[2].linear.bias
    b4 = net[3].linear.bias

    #u, s4, v = torch.svd(W4)
    #print(s4)

    ###### Let us now optimally project ######
    DD  = NNData_In[0:15000,:]
    DDt = DD[:,0:2].T
    KK1 = ((W1@(DD.T)).T+b1).T
    XD2 = nn.functional.relu(KK1)
    KK2 = ((W2@XD2).T+b2).T
    XD3 = nn.functional.relu(KK2)
    KK3 = ((W3@XD3).T+b3).T
    XD4 = nn.functional.relu(KK3)

    W1in = W1[:,0:2]
    W1n  = SvdProj(DDt,W1in,1)
    W1t  = torch.nn.Parameter(torch.from_numpy(W1n).float())
    W1k   = torch.cat((W1[:,0:2],torch.from_numpy(W1n).float()),1)
    W1   = torch.nn.Parameter(W1k)
    W2n = SvdProj(XD2,W2,2)
    W2  = torch.nn.Parameter(torch.from_numpy(W2n).float())
    W3n = SvdProj(XD3,W3,2)
    W3  = torch.nn.Parameter(torch.from_numpy(W3n).float())

    #W4n = SvdProj(XD4,W4,3)
    #W4  = torch.nn.Parameter(torch.from_numpy(W3n).float())

    # Define net
    AC  = 1
    IW  = 1
    net = torch.nn.Sequential(ReluInput(Input_size,W_size,AC,IW,W1,b1,EVR),
                              ReluLayer(W_size,AC,IW,W2,b2,EVR),
                              ReluLayer(W_size,AC,IW,W3,b3,EVR),
                              ReluOutput(Output_size,W_size,AC,IW,W4,b4,EVR))
    ##########################################

# RE-Optimize (Constrained)
opt = torch.optim.Adam(net.parameters(), lr=1e-3)
it_cnt = 1
error_vec = np.empty([it_cnt])
for kk in range(it_cnt):
    opt.zero_grad()
    l = torch.nn.MSELoss()(net(NNData_In), NNData_Out)
    print(kk)
    print (l.item())
    error_vec[kk] = l.item()
    l.backward()
    opt.step()
#
# Once more:
opt.zero_grad()
l = torch.nn.MSELoss()(net(NNData_In), NNData_Out)
print(l.item())
l.backward()

# Save the NN model
PATH = "cdNN.pkl"
#torch.save(net.state_dict(),PATH)

'''W1 = net[0].linear.weight
W2 = net[1].linear.weight
W3 = net[2].linear.weight
W4 = net[3].linear.weight
b1 = net[0].linear.bias
b2 = net[1].linear.bias
b3 = net[2].linear.bias
b4 = net[3].linear.bias
u, s1, v = torch.svd(W1[:,0:Input_size])
u, s2, v = torch.svd(W2)
u, s3, v = torch.svd(W3)
u, s4, v = torch.svd(W4)
print(s1)
print(s2)
print(s3)
print(s4)'''

# Plot Results
fig, axs = plt.subplots(2)

axs[0].plot(net(NNData_In).detach().numpy(),color='orange')
axs[0].plot(NNData_Out.detach().numpy(),color='blue')
axs[1].plot(error_vec)

plt.show()
