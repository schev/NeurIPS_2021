from cvxpylayers.torch import CvxpyLayer
import matplotlib.pyplot as plt
import torch
import pickle
import copy
import time
import cvxpy as cp
import numpy as np
import torch.nn as nn

from SVD_Proj    import SvdProj

# Ensure LAPACK and BLAS are included (Only in Anaconda...)
# # # # np.show_config()

# Define 3 Layers: Input, Middle, and Output
class ReluInput(torch.nn.Module):
    def __init__(self, Input_size, W_size, AC, IW, W, b, EVR):
        super(ReluInput, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(2*Input_size, W_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((W_size, Input_size))
        I  = EVR*torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc[:,0:Input_size]-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])
        self.ReLU = nn.ReLU()
        #self.ii   = 1

    def forward(self, x):
        #self.ii = self.ii + 1
        if self.AC == 1:
            #if self.ii%1 == 0:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn[:,0:Input_size])[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn[:,0:Input_size].copy_(Wcn[:,0:Input_size])
            self.linear.weight = Wn
        return self.ReLU(self.linear(x)) # Apply ReLU and Return
class ReluLayer(torch.nn.Module):
    def __init__(self, W_size, AC, IW, W, b, EVR):
        super(ReluLayer, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(W_size, W_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((W_size, W_size))
        x  = cp.Parameter((W_size, 1))
        I  = EVR*torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])
        self.ReLU = nn.ReLU()
        #self.ii = 1

    def forward(self, x):
        #self.ii = self.ii + 1
        if self.AC == 1:
            #if self.ii%1 == 0:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn.copy_(Wcn)
            self.linear.weight = Wn
        return self.ReLU(self.linear(x)) # Apply ReLU and Return
class ReluOutput(torch.nn.Module):
    def __init__(self, Output_size, W_size, AC, IW, W, b, EVR):
        super(ReluOutput, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(W_size, Output_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((Output_size, W_size))
        I  = EVR*torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc[0:Output_size,:]-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])
        #self.ii = 1

    def forward(self, x):
        #self.ii = self.ii + 1
        if self.AC == 1:
            #if self.ii%1 == 0:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn.copy_(Wcn[0:Output_size,:])
            self.linear.weight = Wn
        return self.linear(x) # Linear Layer -- No ReLU!!

# Define Net Size
W_size      = 25
Output_size = 2
Input_size  = 2 # Do not treat the parameters as inputs!

# Define Network
AC = IW = W = b = 0
EVR = 0.999
net = torch.nn.Sequential(ReluInput(Input_size,W_size,AC,IW,W,b,EVR),
                          ReluLayer(W_size,AC,IW,W,b,EVR),
                          ReluLayer(W_size,AC,IW,W,b,EVR),
                          ReluOutput(Output_size,W_size,AC,IW,W,b,EVR))

# Load and parse pickle data
with open("Cubic_Data.pkl", 'rb') as f:
    training_data = pickle.load(f)
Data_Sim   = training_data[0]
Data_SimIn = training_data[1]
Data_Param = training_data[2]
Data_In    = training_data[3]
Data_Out   = training_data[4]

plt.plot(Data_In.T,color='orange')
plt.plot(Data_Out.T,color='blue')

plt.show()


'''fig, axs = plt.subplots(1)
axs[0].plot(Data_In.T,color='orange')
axs[0].plot(Data_In.T,color='blue')
plt.show()'''
