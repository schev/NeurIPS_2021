from cvxpylayers.torch import CvxpyLayer
import torch
import pickle
import cvxpy as cp
import numpy as np
import torch.nn as nn

# Ensure LAPACK and BLAS are included (Only in Anaconda...)
# # # # np.show_config()

# Define 3 Layers: Input, Middle, and Output
class ReluInput(torch.nn.Module):
    def __init__(self, Input_size, W_size, AC, IW, W, b):
        super(ReluInput, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(Input_size, W_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((W_size, Input_size))
        I  = torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc[:,0:Input_size]-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])

    def forward(self, x):
        if self.AC == 1:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn.copy_(Wcn[:,0:Input_size])
            self.linear.weight = Wn
        return nn.functional.relu(self.linear(x)) # Apply ReLU and Return

class ReluLayer(torch.nn.Module):
    def __init__(self, W_size, AC, IW, W, b):
        super(ReluLayer, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(W_size, W_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((W_size, W_size))
        x  = cp.Parameter((W_size, 1))
        I  = torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])

    def forward(self, x):
        if self.AC == 1:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn.copy_(Wcn)
            self.linear.weight = Wn
        return nn.functional.relu(self.linear(x)) # Apply ReLU and Return

class ReluOutput(torch.nn.Module):
    def __init__(self, Output_size, W_size, AC, IW, W, b):
        super(ReluOutput, self).__init__()
        self.AC     = AC
        self.linear = nn.Linear(W_size, Output_size)
        if IW == 1:
            self.linear.weight = W
            self.linear.bias   = b
        Wc = cp.Variable((W_size, W_size))
        W  = cp.Parameter((Output_size, W_size))
        I  = torch.eye(W_size)
        M1 = cp.vstack((I, Wc.T))
        M2 = cp.vstack((Wc,I))
        M  = cp.hstack((M1,M2))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc[0:Output_size,:]-W)), [M>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])

    def forward(self, x):
        if self.AC == 1:
            Wn = self.linear.weight
            Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
            with torch.no_grad():     # Copy SDP solution Wcn into Wn
                Wn.copy_(Wcn[0:Output_size,:])
            self.linear.weight = Wn
        return self.linear(x) # Linear Layer -- No ReLU!!

# Define Net Size
W_size      = 50
Output_size = 2
Input_size  = 2*Output_size

# Define Network
AC = 0
IW = 0
W  = 0
b  = 0
net = torch.nn.Sequential(ReluInput(Input_size,W_size,AC,IW,W,b),
                          ReluLayer(W_size,AC,IW,W,b),
                          ReluLayer(W_size,AC,IW,W,b),
                          ReluLayer(W_size,AC,IW,W,b),
                          ReluLayer(W_size,AC,IW,W,b),
                          ReluOutput(Output_size,W_size,AC,IW,W,b))

# Load and parse pickle data
with open("Cubic_Data.pkl", 'rb') as f:
    training_data = pickle.load(f)
Data_Sim   = training_data[0]
Data_SimIn = training_data[1]
Data_Param = training_data[2]
Data_In    = training_data[3]
Data_Out   = training_data[4]

# Concatenate, transpose, and prepare the data
NNData_In  = torch.from_numpy(np.concatenate((Data_In,Data_Param),0).transpose()).float()
NNData_Out = torch.from_numpy(Data_Out.transpose()).float()

# Print Parameters
'''for param in net.parameters():
      print(param.data)'''

# Optimize (Unconstrained)
opt = torch.optim.Adam(net.parameters(), lr=5e-3)
for kk in range(100):
    opt.zero_grad()
    l = torch.nn.MSELoss()(net(NNData_In), NNData_Out)
    print(kk)
    print (l.item())
    l.backward()
    opt.step()
#
# Once more:
opt.zero_grad()
l = torch.nn.MSELoss()(net(NNData_In), NNData_Out)
print(l.item())
l.backward()

# Export Weights and Biases
W1 = net[0].linear.weight
W2 = net[1].linear.weight
W3 = net[2].linear.weight
W4 = net[3].linear.weight
W5 = net[4].linear.weight
W6 = net[5].linear.weight
b1 = net[0].linear.bias
b2 = net[1].linear.bias
b3 = net[2].linear.bias
b4 = net[3].linear.bias
b5 = net[4].linear.bias
b6 = net[5].linear.bias

# Limit SVDs
u, s1, v = torch.svd(W1)
for ij in range(len(s1)):
    if s1[ij] > 1:
        s1[ij] = 1
W1 = torch.nn.Parameter(u@torch.diag(s1)@torch.transpose(v,1,0))

u, s2, v = torch.svd(W2)
for ij in range(len(s2)):
    if s2[ij] > 1:
        s2[ij] = 1
W2 = torch.nn.Parameter(u@torch.diag(s2)@torch.transpose(v,1,0))

u, s3, v = torch.svd(W3)
for ij in range(len(s3)):
    if s3[ij] > 1:
        s3[ij] = 1
W3 = torch.nn.Parameter(u@torch.diag(s3)@torch.transpose(v,1,0))

u, s4, v = torch.svd(W4)
for ij in range(len(s4)):
    if s4[ij] > 1:
        s4[ij] = 1
W4 = torch.nn.Parameter(u@torch.diag(s4)@torch.transpose(v,1,0))

u, s5, v = torch.svd(W5)
for ij in range(len(s5)):
    if s5[ij] > 1:
        s5[ij] = 1
W5 = torch.nn.Parameter(u@torch.diag(s5)@torch.transpose(v,1,0))

u, s6, v = torch.svd(W6)
for ij in range(len(s6)):
    if s6[ij] > 1:
        s6[ij] = 1
W6 = torch.nn.Parameter(u@torch.diag(s6)@torch.transpose(v,1,0))

#print(s1)
#print(s2)
#print(s3)
#print(s4)
#print(s5)
#print(s6)

# RE-Define Network
AC = 1
IW = 1
net = torch.nn.Sequential(ReluInput(Input_size,W_size,AC,IW,W1,b1),
                          ReluLayer(W_size,AC,IW,W2,b2),
                          ReluLayer(W_size,AC,IW,W3,b3),
                          ReluLayer(W_size,AC,IW,W4,b4),
                          ReluLayer(W_size,AC,IW,W5,b5),
                          ReluOutput(Output_size,W_size,AC,IW,W6,b6))

# RE-Optimize (Unconstrained)
opt = torch.optim.Adam(net.parameters(), lr=2e-3)
#opt = torch.optim.Adam(net.parameters(), lr=1e-2)
for kk in range(500):
    opt.zero_grad()
    l = torch.nn.MSELoss()(net(NNData_In), NNData_Out)
    print(kk)
    print (l.item())
    l.backward()
    opt.step()
#
# Once more:
opt.zero_grad()
l = torch.nn.MSELoss()(net(NNData_In), NNData_Out)
print(l.item())
l.backward()
