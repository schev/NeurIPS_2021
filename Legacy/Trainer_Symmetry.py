from cvxpylayers.torch import CvxpyLayer
import torch
import cvxpy as cp
import numpy as np
import torch.nn as nn

# Ensure LAPACK and BLAS are included (Only in Anaconda...)
np.show_config()

# Define 3 Layers: Input, Middle, and Output

class ReluInput(torch.nn.Module):
    def __init__(self, dim):
        super(ReluLayer, self).__init__()
        self.linear = nn.Linear(dim, dim)
        self.linear.weight = torch.nn.Parameter(torch.eye(dim))
        Wc   = cp.Variable((dim, dim),symmetric=True)
        W    = cp.Parameter((dim, dim))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc[:,0:Output_size]-W)), [(Wc-torch.eye(dim))<<0,(Wc+torch.eye(dim))>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])

    def forward(self, x):
        Wn = self.linear.weight
        Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
        with torch.no_grad():     # Copy SDP solution Wcn into Wn
            Wn.copy_(Wcn[:,0:Output_size])
        self.linear.weight = Wn
        return nn.functional.relu(self.linear(x)) # Apply ReLU and Return

class ReluLayer(torch.nn.Module):
    def __init__(self, W_size):
        super(ReluLayer, self).__init__()
        self.linear = nn.Linear(W_size, W_size)
        self.linear.weight = torch.nn.Parameter(torch.eye(W_size))
        Wc   = cp.Variable((W_size, W_size),symmetric=True)
        W    = cp.Parameter((W_size, W_size))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc-W)), [(Wc-torch.eye(W_size))<<0,(Wc+torch.eye(W_size))>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])

    def forward(self, x):
        Wn = self.linear.weight
        Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
        with torch.no_grad():     # Copy SDP solution Wcn into Wn
            Wn.copy_(Wcn)
        self.linear.weight = Wn
        return nn.functional.relu(self.linear(x)) # Apply ReLU and Return

class ReluOutput(torch.nn.Module):
    def __init__(self, Output_size, W_size):
        super(ReluLayer, self).__init__()
        self.linear = nn.Linear(W_size, Output_size)
        Wc   = cp.Variable((W_size, W_size),symmetric=True)
        W    = cp.Parameter((Output_size, W_size))
        prob = cp.Problem(cp.Minimize(cp.sum_squares(Wc[0:Output_size,:]-W)), [(Wc-torch.eye(dim))<<0,(Wc+torch.eye(dim))>>0])
        self.layerWc = CvxpyLayer(prob, [W], [Wc])

    def forward(self, x):
        Wn = self.linear.weight
        Wcn = self.layerWc(Wn)[0] # Solve SDP, defined in __init__
        with torch.no_grad():     # Copy SDP solution Wcn into Wn
            Wn.copy_(Wcn[0:Output_size,:])
        self.linear.weight = Wn
        return self.linear(x) # Linear Layer -- No ReLU!!

# Define Net Size
W_size      = 50
Output_size = 2
Input_size  = 2*Output_size

# Define Network
net = torch.nn.Sequential(ReluInput(Input_size,W_size),
                          ReluLayer(W_size),
                          ReluLayer(W_size),
                          ReluLayer(W_size),
                          ReluLayer(W_size),
                          ReluOutput(Output_size,W_size))

# Load pickle data
with open("Cubic_Data.pkl", 'rb') as f:
        training_data = pickle.load(f)








X = torch.randn(300, dim)
Y = torch.randn(300, dim)

print(X)
'''
for param in net.parameters():
  print(param.data)

opt = torch.optim.Adam(net.parameters(), lr=1e-2)
for _ in range(100):
    opt.zero_grad()
    l = torch.nn.MSELoss()(net(X), Y)
    print (l.item())
    l.backward()
    opt.step()

opt.zero_grad()
l = torch.nn.MSELoss()(net(X), Y)
print (l.item())
l.backward()

h = net.parameters()

Wv1 = net[0].linear.weight
Wv2 = net[1].linear.weight
Wv3 = net[2].linear.weight
Wv4 = net[3].linear.weight
Wv5 = net[4].linear.weight

e1,v1 = torch.eig(Wv1)
e2,v2 = torch.eig(Wv2)
e3,v3 = torch.eig(Wv3)
e4,v4 = torch.eig(Wv4)
e5,v5 = torch.eig(Wv5)

print(e1)
print(e2)
print(e3)
print(e4)
print(e5)

for param in net.parameters():
  print(param.data)'''
