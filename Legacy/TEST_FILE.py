import numpy as np
import pickle
import matplotlib.pyplot as plt
import torch.nn as nn
import time

#from oct2py import Oct2Py

import cvxpy as cp
import numpy.random as npr
import scipy.sparse as sp
import torch
from cvxpylayers.torch.cvxpylayer import CvxpyLayer


'''import matlab.engine
eng = matlab.engine.start_matlab()
#tf  = eng.isprime(37)
#F = eng.eig(eng.randn((5)))
g = eng.mat_func(5)
print(g)
eng.quit()'''

# Load and parse pickle data
with open("Cubic_Data.pkl", 'rb') as f:
    training_data = pickle.load(f)
Data_Sim   = training_data[0]
Data_SimIn = training_data[1]
Data_Param = training_data[2]
Data_In    = training_data[3]
Data_Out   = training_data[4]

plt.plot(Data_In.T)
plt.plot(Data_Out.T)
plt.plot(Data_Param.T)
plt.show()

x0  = np.array([0.5, -0.5])
x0p = x0 + 0.25*np.random.randn(2)



'''W_size = 5
Output_size = 5
WW = torch.randn(Output_size, W_size)
print(WW)
print(WW[0:2,:])'''

'''W_size      = 5
Output_size = 2
Wc   = cp.Variable((W_size, W_size),symmetric=True)
W    = cp.Parameter((Output_size, W_size))
problem = cp.Problem(cp.Minimize(cp.sum_squares(Wc[0:Output_size,:]-W)), [(Wc-torch.eye(W_size))<<0,(Wc+torch.eye(W_size))>>0])
cvxpl   = CvxpyLayer(problem, parameters=[W], variables=[Wc])
Wnn       = 0.1*torch.randn(Output_size, W_size)
solution, = cvxpl(Wnn)
print(Wnn)
print(solution)

I    = torch.eye(3)
A = [[I,I],[I,I]]
G = np.bmat([[I, I], [I, I]])'''
#print(A)
#print(G)


'''W_size   = 50
dat_size = 100
Wc  = cp.Variable((W_size, W_size))
Wc  = cp.Variable((W_size, W_size))
v   = cp.Parameter((W_size,dat_size))
X   = cp.Parameter((W_size,dat_size))

I  = torch.eye(W_size)
M1 = cp.vstack((I, Wc.T))
M2 = cp.vstack((Wc,I))
M  = cp.hstack((M1,M2))

#problem = cp.Problem(cp.Minimize(cp.sum_squares(Wc@X-v)),[])
problem = cp.Problem(cp.Minimize(cp.norm(Wc@X-v)))
#problem = cp.Problem(cp.Minimize(cp.sum_squares(Wc@X-v)),[M>>0])
#problem = cp.Problem(cp.Minimize(cp.sum_squares(Wc@X-v)))

cvxpl   = CvxpyLayer(problem, parameters=[X,v], variables=[Wc])

Wn = torch.randn(W_size, W_size)
Xn = torch.randn(W_size, dat_size)
vn = Wn@Xn

start = time.time()
problem.solve(Xn,vn)
solution, = cvxpl(Xn,vn)
end = time.time()
print(end - start)

print(Wn)
print(solution)

print(solution-Wn)'''


'''u, s1, v = torch.svd(Wn)
u, s2, v = torch.svd(solution)
print(s1)
print(s2)'''




'''W_size = 2
W1   = cp.Variable((W_size, W_size),symmetric=True)
W2   = cp.Variable((W_size, W_size),symmetric=True)
W    = [[2,3], [4,5]]
#W    = cp.Parameter((W_size, W_size))

problem = cp.Problem(cp.Minimize(cp.sum_squares(W-W1@W2)))
sol, = problem.solve()
#Wn        = torch.randn(W_size, W_size)
#solution, = cvxpl(Wn)'''




'''
print(A)


Kv      = np.empty([2,4])
x0      = np.array([0.8, -0.4])
Kv[:,0] = x0
Kv[:,1] = x0
Kv[:,2] = x0

#h = np.append(Kv,x0,axis=0)

B = np.reshape(x0, (2, 1))
h = np.concatenate((Kv, B),1)

#print(B)
#print(Kv)
#print(len(h[1,:]))

with open("Cubic_Data.pkl", 'rb') as f:
        #pickle.dump(Data_Sim, f)
        data = pickle.load(f)

print(data[2])

#plt.plot(training_data[1,:])
#plt.show()


#print(Kv[0,3:2])

#for ii in range(2):
#    print(ii)'''

'''

#print(Kv)

ind = [8, 9]

print(np.arange(2))

#Kv = np.delete(Kv, np.arange(io,5*(len(tspan)+1)), 1)

#print(Kv)

np.random.seed(1)
gg = 0.25*np.random.randn(100)
plt.plot(gg)
plt.show()'''

'''W_size   = 50
dat_size = 500
Wc  = cp.Variable((W_size, W_size))
Wn = 0.5*torch.randn(W_size, W_size)
Xn = torch.randn(W_size, dat_size)
vn = Wn@Xn
I  = torch.eye(W_size)
M1 = cp.vstack((I, Wc.T))
M2 = cp.vstack((Wc,I))
M  = cp.hstack((M1,M2))
problem = cp.Problem(cp.Minimize(cp.norm(Wc@Xn-vn)),[M>>0])
start  = time.time()
problem.solve(solver=cp.SCS,eps=1e-2)
end    = time.time()
print(end - start)

#print(Wn)
print(problem.value)
#print(Wc.value)

u, s1, v = torch.svd(Wn)
u, s2, v = torch.svd(torch.from_numpy(Wc.value))
print(s1)
print(s2)'''

'''
dat_size = 100
Wc  = cp.Variable((W_size, W_size))
Wn = 0.5*torch.randn(W_size, W_size)
Xn = torch.randn(W_size, dat_size)
vn = Wn@Xn
I  = torch.eye(W_size)
M1 = cp.vstack((I, Wc.T))
M2 = cp.vstack((Wc,I))
M  = cp.hstack((M1,M2))
problem = cp.Problem(cp.Minimize(cp.norm(Wc@Xn-vn)),[M>>0])
start  = time.time()
problem.solve(solver=cp.SCS,eps=1e-2)
end    = time.time()
print(end - start)

#print(Wn)
print(problem.value)
u, s1, v = torch.svd(Wn)
u, s2, v = torch.svd(torch.from_numpy(Wc.value))
print(s1)
print(s2)'''

# Build Alternative
'''W_size   = 3
dat_size = 8
Wn = torch.randn(W_size, W_size)
Xn = torch.randn(W_size, dat_size)
A  = torch.zeros((W_size*dat_size,W_size*W_size))
B  = Wn@Xn
print(B)
b = B.flatten()
print(b)
x = Wn.flatten()


print(Wn)
print(x)

#i1 = 1*W_size*dat_size
#print(A[ + 0:(W_size*dat_size),0])

for ii in range(W_size):
    #i1 = 0               + ii*W_size*dat_size
    #i2 = W_size*dat_size + ii*W_size*dat_size
    #A[i1:i2,ii] = torch.reshape(Xn.T, (W_size*dat_size,1)).flatten()
    i1 = 0               + ii*dat_size
    i2 =    dat_size     + ii*dat_size
    i3 = 0               + ii*W_size
    i4 =    W_size       + ii*W_size

    A[i1:i2,i3:i4] = Xn.T
    print(A[i1:i2,i3:i4])

print(A@x)
print(b)
print(A@x-b)

#print(x.size())
#print(b.size())
#plt.spy(A)
#plt.show()
u, s1, v = torch.svd(A)
#u, s1, v = torch.svd_lowrank(A,q=2)

S = torch.diag(s1)

bu = u.T@b

print(u@S@(v.T@x)-b)

bu = u.T@b
MM = S@v.T
print(MM@x-bu)

print(MM.size())
print(bu.size())



xm  = cp.Variable((W_size, W_size))
xv  = cp.vec(xm.T)
#print(xv)
I  = torch.eye(W_size)
M1 = cp.vstack((I, xm.T))
M2 = cp.vstack((xm,I))
M  = cp.hstack((M1,M2))

#problem = cp.Problem(cp.Minimize(cp.norm(MM@xv-bu)),[M>>0])
problem = cp.Problem(cp.Minimize(cp.sum_squares(MM@xv-bu)),[M>>0])
res = problem.solve()

print(xm.value)
print(Wn)

u, s1, v = torch.svd(A)
#print(u)

u, s1, v = torch.svd(A,some=True)

u, s1, v = torch.svd_lowrank(A, q=1)

Q, R = torch.qr(A, some=True)

print(Q.size())
print(R.size())
print(MM.size())'''

#print(s2)

#print(u.size())
# new min: A@x - bu
